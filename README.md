# BalaBox : Application de suivi d'activités sportives

Ce dépôt contient le code source de l'application de suivi d'activités
sportive intégrée à la [BalaBox], ainsi que la documentation y
afférente.

## Application de suivi d'activités sportives
L'application de suivi d'activités sportives intégrée à la [BalaBox]
permet à l'enseignant d'éducation physique et sportive de créer des
activités et d'évaluer les élèves lors de celles-ci. Cette application
s'inspire en partie du projet [EPSBox]. Les élèves peuvent saisir
leurs résultats sur de terminaux mobiles. Les activités peuvent être
des sports collectifs ou individuels.


## Liste des fonctionnalités offertes par le tableau de bord
* Création, modification et suppression des activités par l'enseignant
* Saisie des résultats par l'enseignant ou les élèves via des.
  terminaux numériques.
* Identification automatique de l'élève à partir du service d' [identification].
* Exportation des résultats et évaluations, et stockage des résultats
  et évaluations sur un support externe.

## Mise en œuvre du tableau de bord
Le service est composé de deux interfaces utilisateurs: un interface
pour l'enseignant et une interface pour les élèves.

Le service exploite le service d' [identification] de la [BalaBox] pour
identifier l'élève, ou un groupe d'élèves.


Pour assurer un partage et la diffusion en direct des contenus
numériques, les interfaces utilisateur des élèves et de l'enseignant
reposent sur des WebSockets.

[balabox]: https://balabox.gitlab.io/balabox/
[moodlebox]: https://moodlebox.net
[epsbox]: http://epsbox.free.fr/#page-content
[identification]: https://balabox.gitlab.io/identification/
